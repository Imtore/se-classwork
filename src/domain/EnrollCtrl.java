package domain;

import java.util.List;
import domain.exceptions.EnrollmentRulesViolationException;

public class EnrollCtrl {
	public void enroll(Student student, List<Offering> offeringList) throws EnrollmentRulesViolationException {

		for (Offering offering : offeringList) {
			prerequisiteCotnrol(offering, student);
			doublePassControl(offering, student);
		}
		timeOverlapControl(offeringList);
		doubleEnrollControl(offeringList);
		checkLimitbyGPA(offeringList, student);
		for (Offering o : offeringList)
			student.takeOffering(o);
	}

	private void prerequisiteCotnrol(Offering offering, Student student) throws EnrollmentRulesViolationException {
		List<Course> prereqs = offering.getCourse().getPrerequisites();
		nextPre: for (Course pre : prereqs) {
			List<StudyRecord> transcript = student.getStudyRecords();
			for (StudyRecord sr : transcript) {
				if (sr.getOffering().getCourse().equals(pre) && sr.getGrade() >= 10)
					continue nextPre;
			}
			throw new EnrollmentRulesViolationException(
					String.format("The student has not passed %s as a prerequisite of %s", pre.getName(),
							offering.getCourse().getName()));
		}
	}

	private void doublePassControl(Offering offering, Student student) throws EnrollmentRulesViolationException {
		List<StudyRecord> transcript = student.getStudyRecords();
		for (StudyRecord sr : transcript) {
			if (sr.getOffering().getCourse().equals(offering.getCourse()) && sr.getGrade() >= 10)
				throw new EnrollmentRulesViolationException(
						String.format("The student has already passed %s", offering.getCourse().getName()));
		}
	}

	private void timeOverlapControl(List<Offering> offeringList) throws EnrollmentRulesViolationException {
		for (Offering offering : offeringList) {
			for (Offering eachOffering : offeringList) {
				if (offering == eachOffering)
					continue;
				if (offering.getExamTime().equals(eachOffering.getExamTime()))
					throw new EnrollmentRulesViolationException(
							String.format("Two offerings %s and %s have the same exam time", offering, eachOffering));
			}
		}
	}

	private void doubleEnrollControl(List<Offering> offeringList) throws EnrollmentRulesViolationException {
		for (Offering offering : offeringList) {
			for (Offering eachOffering : offeringList) {
				if (offering == eachOffering)
					continue;
				if (offering.getCourse().equals(eachOffering.getCourse()))
					throw new EnrollmentRulesViolationException(
							String.format("%s is requested to be taken twice", offering.getCourse().getName()));
			}
		}
	}

	private void checkLimitbyGPA(List<Offering> offeringList, Student student)
			throws EnrollmentRulesViolationException {
		int unitsRequested = 0;
		for (Offering offering : offeringList)
			unitsRequested += offering.getUnits();
		double points = 0;
		int totalUnits = 0;
		for (StudyRecord sr : student.getStudyRecords()) {
			points += sr.getGrade() * sr.getUnits();
			totalUnits += sr.getUnits();
		}
		double gpa = points / totalUnits;
		if ((gpa < 12 && unitsRequested > 14) || (gpa < 16 && unitsRequested > 16) || (unitsRequested > 20))
			throw new EnrollmentRulesViolationException(
					String.format("Number of units (%d) requested does not match GPA of %f", unitsRequested, gpa));
	}
}
